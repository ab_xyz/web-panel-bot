package xyz.akhiltay.webpanelbot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xyz.akhiltay.webpanelbot.entities.Session;

import java.util.Optional;

public interface UserSessionRepository extends JpaRepository<Session, Integer> {
    Optional<Session> findBySession(String sessionId);
}
