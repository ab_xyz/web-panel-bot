    package xyz.akhiltay.webpanelbot.repositories;

    import org.springframework.data.jpa.repository.JpaRepository;
    import xyz.akhiltay.webpanelbot.entities.User;

    import java.util.Optional;

    public interface UserRepository extends JpaRepository<User, Integer> {
        Optional<User> findByUsername(String username);
        Optional<User> findByTelegramId(Long telegramId);
    }
