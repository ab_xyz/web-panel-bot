package xyz.akhiltay.webpanelbot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xyz.akhiltay.webpanelbot.entities.Subscription;

import java.util.Optional;

public interface SubscriptionRepository extends JpaRepository<Subscription, Integer> {
    Optional<Subscription> findById(long id);
}
