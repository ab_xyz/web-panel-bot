package xyz.akhiltay.webpanelbot.filters;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import xyz.akhiltay.webpanelbot.repositories.UserRepository;
import xyz.akhiltay.webpanelbot.repositories.UserSessionRepository;

import java.io.IOException;
import java.time.Instant;
import java.util.Collections;

@Component
public class SessionFilter extends OncePerRequestFilter {
    @Autowired
    private UserSessionRepository sessionRepository;

    @Autowired
    private UserRepository userRepository;


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        HttpSession httpSession = request.getSession(false);
        String sessionString = (String) (httpSession != null ? httpSession.getAttribute("session") : null);

        if (sessionString == null) {
            sessionString = request.getParameter("session");
        }

        if (sessionString != null) {
            var session = sessionRepository.findBySession(sessionString);
            if (session.isPresent() && session.get().getExpiresAt().isAfter(Instant.now())) {
                var user = session.get().getUser();
                var auth = new UsernamePasswordAuthenticationToken(user, null, Collections.emptyList());
                auth.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(auth);
                if (httpSession != null) {
                    httpSession.setAttribute("session", sessionString);
                }
                System.out.println("User authenticated: " + user.getUsername());
            } else {
                System.out.println("Invalid or expired session");
            }
        } else {
            System.out.println("No session found in request");
        }

        filterChain.doFilter(request, response);
    }


}
