package xyz.akhiltay.webpanelbot.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.akhiltay.webpanelbot.entities.User;
import xyz.akhiltay.webpanelbot.repositories.UserSessionRepository;
import xyz.akhiltay.webpanelbot.repositories.SubscriptionRepository;
import xyz.akhiltay.webpanelbot.repositories.UserRepository;

import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private UserSessionRepository sessionRepository;

    public User registerUser(Long telegramId, Optional<String> usernameOpt, Optional<String> nameOpt, String phone) {
        User user = userRepository.findByTelegramId(telegramId)
                .orElseGet(() -> {
                    User newUser = new User();
                    newUser.setTelegramId(telegramId);
                    newUser.setUsername(usernameOpt.orElse(null));
                    newUser.setName(nameOpt.orElse(null));
                    newUser.setPhone(phone);
                    return userRepository.save(newUser);
                });

        usernameOpt.ifPresent(username -> {
            user.setUsername(username);
            userRepository.save(user);
        });

        return user;
    }
}
