package xyz.akhiltay.webpanelbot.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.akhiltay.webpanelbot.entities.Session;
import xyz.akhiltay.webpanelbot.entities.User;
import xyz.akhiltay.webpanelbot.repositories.UserRepository;
import xyz.akhiltay.webpanelbot.repositories.UserSessionRepository;

import java.security.SecureRandom;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Base64;

@Service
public class SessionService {

    @Autowired
    private UserSessionRepository sessionRepository;

    @Autowired
    private UserRepository userRepository;

    private static final SecureRandom secureRandom = new SecureRandom();
    private static final Base64.Encoder base64Encoder = Base64.getUrlEncoder();

    public Session createSession(Integer userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user ID: " + userId));

        String sessionString = generateNewSessionString();
        Instant now = Instant.now();
        Instant expiresAt = now.plus(1, ChronoUnit.HOURS);  // Session expires in 1 hour

        Session session = new Session();
        session.setUser(user);
        session.setSession(sessionString);
        session.setExpiresAt(expiresAt);
        session.setCreatedAt(now);
        return sessionRepository.save(session);
    }

    private String generateNewSessionString() {
        byte[] randomBytes = new byte[24];
        secureRandom.nextBytes(randomBytes);
        return base64Encoder.encodeToString(randomBytes);
    }
}
