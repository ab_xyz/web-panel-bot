package xyz.akhiltay.webpanelbot.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.akhiltay.webpanelbot.entities.Subscription;
import xyz.akhiltay.webpanelbot.entities.User;
import xyz.akhiltay.webpanelbot.repositories.SubscriptionRepository;
import xyz.akhiltay.webpanelbot.repositories.UserRepository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
public class SubscriptionService {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionService.class);

    private final SubscriptionRepository subscriptionRepository;
    private final UserRepository userRepository;

    public SubscriptionService(SubscriptionRepository subscriptionRepository, UserRepository userRepository) {
        this.subscriptionRepository = subscriptionRepository;
        this.userRepository = userRepository;
    }

    public List<Subscription> findAll() {
        return subscriptionRepository.findAll();
    }

    public Optional<Subscription> findById(Integer id) {
        return subscriptionRepository.findById(id);
    }

    public Subscription save(Subscription subscription) {
        return subscriptionRepository.save(subscription);
    }

    public void deleteById(Integer id) {
        subscriptionRepository.deleteById(id);
    }

    @Transactional
    public void subscribeUser(Long telegramId, Integer subscriptionId) {
        logger.info("Attempting to subscribe user with telegramId: {} to subscription: {}", telegramId, subscriptionId);
        Optional<User> userOpt = userRepository.findByTelegramId(telegramId);
        Optional<Subscription> subscriptionOpt = subscriptionRepository.findById(subscriptionId);

        if (userOpt.isPresent() && subscriptionOpt.isPresent()) {
            User user = userOpt.get();
            Subscription subscription = subscriptionOpt.get();
            if (user.getSubscriptions().contains(subscription)) {
                logger.info("User with telegramId: {} is already subscribed to subscription: {}", telegramId, subscriptionId);
            } else {
                user.getSubscriptions().add(subscription);
                userRepository.save(user);
                logger.info("User with telegramId: {} successfully subscribed to subscription: {}", telegramId, subscriptionId);
            }
        } else {
            logger.warn("User or Subscription not found for telegramId: {} and subscriptionId: {}", telegramId, subscriptionId);
        }
    }

    @Transactional
    public void unsubscribeUser(Long telegramId, Integer subscriptionId) {
        logger.info("Attempting to unsubscribe user with telegramId: {} from subscription: {}", telegramId, subscriptionId);
        Optional<User> userOpt = userRepository.findByTelegramId(telegramId);
        Optional<Subscription> subscriptionOpt = subscriptionRepository.findById(subscriptionId);

        if (userOpt.isPresent() && subscriptionOpt.isPresent()) {
            User user = userOpt.get();
            Subscription subscription = subscriptionOpt.get();
            if (user.getSubscriptions().contains(subscription)) {
                user.getSubscriptions().remove(subscription);
                userRepository.save(user);
                logger.info("User with telegramId: {} successfully unsubscribed from subscription: {}", telegramId, subscriptionId);
            } else {
                logger.info("User with telegramId: {} is not subscribed to subscription: {}", telegramId, subscriptionId);
            }
        } else {
            logger.warn("User or Subscription not found for telegramId: {} and subscriptionId: {}", telegramId, subscriptionId);
        }
    }

    @Transactional
    public boolean isUserSubscribed(Long telegramId, Integer subscriptionId) {
        Optional<User> userOpt = userRepository.findByTelegramId(telegramId);
        Optional<Subscription> subscriptionOpt = subscriptionRepository.findById(subscriptionId);

        if (userOpt.isPresent() && subscriptionOpt.isPresent()) {
            User user = userOpt.get();
            Subscription subscription = subscriptionOpt.get();
            return user.getSubscriptions().contains(subscription);
        }
        return false;
    }

    @Transactional
    public void createTestSubscription() {
        Subscription testSubscription = new Subscription();
        testSubscription.setName("Test Subscription");
        subscriptionRepository.save(testSubscription);
    }
}
