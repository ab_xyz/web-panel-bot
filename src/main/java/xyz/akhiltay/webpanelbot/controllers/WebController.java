package xyz.akhiltay.webpanelbot.controllers;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import xyz.akhiltay.webpanelbot.entities.User;

@Controller
public class WebController {
    @Value("${telegram.username}")
    private String botUsername;

    @GetMapping("/dashboard")
    public String getDashboard(Authentication authentication, Model model) {
        if (authentication == null || !(authentication.getPrincipal() instanceof User)) {
            return "redirect:/login";
        }

        var user = (User) authentication.getPrincipal();
        model.addAttribute("user", user);
        return "dashboard";
    }


    @GetMapping("/login")
    public String getLogin(Model model) {
        model.addAttribute("botUsername", botUsername);
        return "login";
    }

    @GetMapping("/login/callback")
    public String handleLoginCallback(@RequestParam String session, HttpServletRequest request, Model model) {
        HttpSession httpSession = request.getSession();
        httpSession.setAttribute("session", session);
        request.setAttribute("session", session);
        model.addAttribute("session", session);
        return "redirect:/dashboard";
    }
}
