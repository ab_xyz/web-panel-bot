package xyz.akhiltay.webpanelbot.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import xyz.akhiltay.webpanelbot.entities.Subscription;
import xyz.akhiltay.webpanelbot.entities.User;
import xyz.akhiltay.webpanelbot.services.SubscriptionService;

import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/subscriptions")
public class SubscriptionController {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionController.class);

    private final SubscriptionService subscriptionService;

    @Autowired
    public SubscriptionController(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    private Long getTelegramId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() instanceof User) {
            return ((User) authentication.getPrincipal()).getTelegramId();
        }
        return null;
    }

    @GetMapping
    public String listSubscriptions(Model model) {
        Long telegramId = getTelegramId();
        if (telegramId == null) {
            model.addAttribute("error", "User not authenticated");
            return "error";
        }

        logger.info("User authenticated: " + telegramId);

        List<Subscription> subscriptions = subscriptionService.findAll();
        Map<Integer, Boolean> subscriptionStatus = subscriptions.stream()
                .collect(Collectors.toMap(
                        Subscription::getId,
                        subscription -> subscriptionService.isUserSubscribed(telegramId, subscription.getId())
                ));

        model.addAttribute("subscriptions", subscriptions);
        model.addAttribute("subscriptionStatus", subscriptionStatus);
        return "subscriptions/list";
    }

    @PostMapping("/subscribe")
    public String subscribe(@RequestParam Integer subscriptionId, Model model) {
        Long telegramId = getTelegramId();
        if (telegramId == null) {
            model.addAttribute("error", "User not authenticated");
            return "error";
        }

        subscriptionService.subscribeUser(telegramId, subscriptionId);
        logger.info("User {} subscribed to subscription {}", telegramId, subscriptionId);
        return "redirect:/subscriptions";
    }

    @PostMapping("/unsubscribe")
    public String unsubscribe(@RequestParam Integer subscriptionId, Model model) {
        Long telegramId = getTelegramId();
        if (telegramId == null) {
            model.addAttribute("error", "User not authenticated");
            return "error";
        }

        subscriptionService.unsubscribeUser(telegramId, subscriptionId);
        logger.info("User {} unsubscribed from subscription {}", telegramId, subscriptionId);
        return "redirect:/subscriptions";
    }
}
