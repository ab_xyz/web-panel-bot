package xyz.akhiltay.webpanelbot;

import jakarta.annotation.PostConstruct;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.client.okhttp.OkHttpTelegramClient;
import org.telegram.telegrambots.longpolling.BotSession;
import org.telegram.telegrambots.longpolling.interfaces.LongPollingUpdateConsumer;
import org.telegram.telegrambots.longpolling.starter.AfterBotRegistration;
import org.telegram.telegrambots.longpolling.starter.SpringLongPollingBot;
import org.telegram.telegrambots.longpolling.util.LongPollingSingleThreadUpdateConsumer;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.*;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.generics.TelegramClient;
import xyz.akhiltay.webpanelbot.services.SessionService;
import xyz.akhiltay.webpanelbot.services.UserService;

import java.util.Optional;

@Getter
@Component
public class TelegramBot implements SpringLongPollingBot, LongPollingSingleThreadUpdateConsumer {
    private final UserService userService;
    private final SessionService sessionService;
    private TelegramClient telegramClient;

    @Value("${telegram.token}")
    private String botToken;

    @Value("${telegram.server_url}")
    private String serverUrl;

    public TelegramBot(UserService userService, SessionService sessionService) {
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @PostConstruct
    public void init() {
        this.telegramClient = new OkHttpTelegramClient(botToken);
    }

    @Override
    public LongPollingUpdateConsumer getUpdatesConsumer() {
        return this;
    }

    @Override
    public void consume(Update update) {
        if (update.hasMessage() && update.getMessage().getChat().getType().equalsIgnoreCase("private")) {
            if (update.getMessage().isCommand()) {
                handleCommand(update);
            } else if (update.getMessage().hasContact()) {
                handleContact(update);
            } else if (update.getMessage().hasText()) {
                handleMessage(update);
            }
        }
    }

    private void handleCommand(Update update) {
        var command = update.getMessage().getText();
        var chatId = update.getMessage().getChatId();

        if (command.startsWith("/start")) {
            var message = SendMessage.builder()
                    .chatId(chatId)
                    .text("Чтобы авторизоваться на сайте, поделитесь своим номером с ботом.")
                    .replyMarkup(ReplyKeyboardMarkup
                            .builder()
                            .oneTimeKeyboard(true)
                            .resizeKeyboard(true)
                            .keyboardRow(new KeyboardRow(
                                    KeyboardButton
                                            .builder()
                                            .text("Поделиться номером")
                                            .requestContact(true)
                                            .build()
                            ))
                            .build())
                    .build();

            sendMessage(message);
        }
    }

    private void handleContact(Update update) {
        var telegramId = update.getMessage().getChatId();
        var phone = update.getMessage().getContact().getPhoneNumber();

        var username = update.getMessage().getFrom().getUserName();

        var user = userService.registerUser(telegramId, Optional.ofNullable(username), Optional.empty(), phone);

        var session = sessionService.createSession(user.getId()).getSession();

        var message = SendMessage.builder()
                .chatId(telegramId)
                .text("Логин подтвержден! Нажмите на кнопку ниже, чтобы вернуться на сайт.")
                .replyMarkup(InlineKeyboardMarkup
                        .builder()
                        .keyboardRow(
                                new InlineKeyboardRow(
                                        InlineKeyboardButton
                                                .builder()
                                                .text("Вернуться на сайт")
                                                .url(serverUrl + session)
                                                .build()
                                )
                        )
                        .build()
                )
                .build();

        sendMessage(message);
    }


    private void handleMessage(Update update) {
    }

    private void sendMessage(SendMessage message) {
        try {
            telegramClient.execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public void sendTextMessage(Long chatId, String text) {
        SendMessage message = new SendMessage(chatId.toString(), text);
        sendMessage(message);
    }


    @AfterBotRegistration
    public void afterRegistration(BotSession botSession) {
        System.out.println("Registered bot running state is: " + botSession.isRunning());
    }

}
