package xyz.akhiltay.webpanelbot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import xyz.akhiltay.webpanelbot.services.SubscriptionService;

@Component
public class TestDataInitializer implements CommandLineRunner {

    @Autowired
    private SubscriptionService subscriptionService;

    @Override
    public void run(String... args) throws Exception {
        subscriptionService.createTestSubscription();
        System.out.println("Test subscription created.");
    }
}
