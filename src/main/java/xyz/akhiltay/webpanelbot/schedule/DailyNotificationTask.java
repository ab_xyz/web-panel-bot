package xyz.akhiltay.webpanelbot.schedule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import xyz.akhiltay.webpanelbot.services.SubscriptionService;
import xyz.akhiltay.webpanelbot.TelegramBot;

@Component
public class DailyNotificationTask {

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private TelegramBot telegramBot;

        @Scheduled(cron = "0 0 9 * * ?")
        public void sendDailyNotifications() {
            var subscriptions = subscriptionService.findAll();
        for (var subscription : subscriptions) {
            for (var user : subscription.getUsers()) {
                telegramBot.sendTextMessage(user.getTelegramId(), "Your daily notification for " + subscription.getName());
            }
        }
    }
}