package xyz.akhiltay.webpanelbot.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import xyz.akhiltay.webpanelbot.filters.SessionFilter;

@Configuration
public class SecurityConfig {

    @Autowired
    private SessionFilter sessionFilter;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(r ->
                        r
                                .requestMatchers("/login", "/login/callback").permitAll()
                                .requestMatchers("/subscriptions/**").authenticated()
                                .anyRequest().authenticated()
                )
                .addFilterBefore(sessionFilter, UsernamePasswordAuthenticationFilter.class);
        return http.build();
    }
}
